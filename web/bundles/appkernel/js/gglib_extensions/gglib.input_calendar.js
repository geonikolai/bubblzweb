(function ($) {
    $.gglib.Calendar = function (options)
    {
        var me = this;

        me.calendar = undefined;
        me.calendarClassName = 'gglib-calendar';
        me.calendarContainerClassName = 'gglib-calendar-container';
        me.calendarWrapperClassName = 'gglib-calendar-wrapper';
        me.calendarActiveClassName = 'gglib-calendar-active';

        me.selectedDate = undefined;

        //set default options
        me.settings = {
            element: $('.datepicker')
        };

        //Process user options
        $.extend(me.settings, options);

        me.Draw = function () {
            //Create the calendar element
            var calendar = document.createElement("div");
            calendar.className = me.calendarClassName;

            //Create the calendar container so we can show and hide without destroying the calendar
            var calendarContainer = document.createElement("div");
            calendarContainer.className = me.calendarContainerClassName;
            calendarContainer.style.display = 'none';
            calendarContainer.style.position = 'absolute';

            //Create the wrapper to inject the input element and the calendar container
            var wrapper = document.createElement('div');
            wrapper.className = me.calendarWrapperClassName;

            //Append the calendar to its container
            calendarContainer.appendChild(calendar);

            //Append the container to the wrapper
            wrapper.appendChild(calendarContainer);

            //Inject the wrapper exactly before input element
            $(wrapper).insertBefore(me.settings.element);

            //Prepend the input element to the wrapper
            me.settings.element.prependTo($(wrapper));
        };

        me.Show = function () {
            if ((me.settings.element.offset().top > ($(window).scrollTop() + $(window).height() - 100))) {
                $('.' + me.calendarContainerClassName).css('top', me.settings.element.offset().top - 340);
            }
            $('.' + me.calendarContainerClassName).show();
            me.settings.element.addClass(me.calendarActiveClassName);
        };
        me.Hide = function () {
            $('.' + me.calendarContainerClassName).hide();
            me.settings.element.removeClass(me.calendarActiveClassName);
        };

        me.Destroy = function () {
            $(me.calendar).jqxCalendar('destroy');
            me.settings.element.removeClass(me.calendarActiveClassName);
        };

        var handlers = function () {
            me.settings.element.click(function () {
                var $this = $(this);
                if ($this.hasClass(me.calendarActiveClassName)) {
                    me.Hide();
                    return;
                }
                me.Show();
            });

            me.calendar.on('change', function (event) {
                var view = event.args.owner.view;
                if (view !== 'month') {
                    return;
                }

                var date = event.args.date;
                var month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth();
                var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
                me.settings.element.val(day + '/' + month + '/' + date.getFullYear());
                me.Hide();
            });
        };

        var build = function () {
            //Draw the calendar
            me.Draw();

            //Create a jqxCalendar
            me.calendar = me.settings.element
                    .closest('.' + me.calendarWrapperClassName)
                    .find('.' + me.calendarClassName)
                    .jqxCalendar({width: 220, height: 200, theme: 'office'});

            handlers();
        };

        build();

        return me;
    }
}(jQuery));