(function ($) {
    $.fn.gglibJqxGrid = function (options)
    {
        var me = this;

        me.grid = undefined;

        //set default options
        me.settings = {
            width: "100%",
            theme: "office",
            sortable: true,
            selectionmode: "singlerow",
            groupable: true,
            columnsresize: true,
            filterable: true,
            pageable: true,
            virtualmode: true,
            rendergridrows: function (obj)
            {
                return obj.data;
            },
            pagesizeoptions: ['100', '200', '500'],
            pagesize: 500,
            pagerButtonsCount: 10,
            source: undefined,
            columnsResize: true,
            columns: undefined,
            altRows: true,
            autoshowfiltericon: true,
            showfilterrow: true,
            ready: function () {
                me.DataGridRowMenu();
            }
        };

        //Process user options
        $.extend(me.settings, options);

        me.hasRowMenu = true;
        me.selectedRowData = undefined;
        me.selectedColumnField = undefined;
        me.deleteUrl = undefined;
        me.canDelete = false;
        me.deleteRowMessage = '';
        me.deleteRowTitle = '';

        me.editUrl = undefined;
        me.canEdit = false;
        me.redirectEdit = false;

        me.groups = [];

        me.refresh = function () {
            me.grid.jqxGrid('updatebounddata');
        };

        me.DataGridRowMenu = function () {
            if (me.hasRowMenu)
            {
                //Define the menu buttons
                var deleteButton = "<li style='white-space: nowrap;'><span class='datarid-row-menu-delete-button'><i class='fa fa-times color-red'></i> Delete row</span></li>";
                var editButton = "<li style='white-space: nowrap;'><span class='datarid-row-menu-edit-button'><i class='fa fa-pencil color-green'></i> Edit row</span></li>";
                var sortDesc = "<li style='white-space: nowrap;'><span class='datarid-row-menu-sort-desc-button'><i class='fa fa-sort-alpha-desc color-green'></i> Sort descending</span></li>";
                var sortAsc = "<li style='white-space: nowrap;'><span class='datarid-row-menu-sort-asc-button'><i class='fa fa-sort-alpha-asc color-green'></i> Sort ascending</span></li>";
                var clearSort = "<li style='white-space: nowrap;'><span class='datarid-row-menu-sort-clear-button'><i class='fa fa-sort-alpha-desc color-green'></i> Clear sort</span></li>";
                var separator = "<li type='separator'></li>";
                var clearFilters = "<li style='white-space: nowrap;'><i class='fa fa-sort-alpha-desc color-red'></i> Clear all filters</li>";
                var clearColumnFilters = "<li style='white-space: nowrap;'><i class='fa fa-sort-alpha-desc color-red'></i> Clear column filters</li>";
                var addColumnToGroups = "<li style='white-space: nowrap;'><span class='datarid-row-menu-add-group-button'><i class='fa fa-sort-alpha-desc color-cyan'></i> Add column to groups</span></li>";

                //Insert buttons to menu
                var menu = "<div class='jqxMenu'>"
                        + "<ul>"
                        + sortAsc
                        + sortDesc
                        + clearSort
                        + separator
                        + addColumnToGroups
                        + separator;
                if (me.canEdit) {
                    menu += editButton + separator;
                }
                if (me.canDelete) {
                    menu += deleteButton;
                }
                menu += clearFilters
                        + clearColumnFilters
                        + "</ul>"
                        + "</div>";

                //Create the mennu
                var contextMenu = $(menu).jqxMenu({width: 'auto', height: 'auto', theme: "office", autoOpenPopup: false, mode: 'popup'});

                //Define row grid row right click event to open the menu
                me.grid.on('rowclick', function (event) {
                    if (event.args.rightclick) {
                        $(me).jqxGrid('selectrow', event.args.rowindex);
                    }

                    var selectedRowIndexes = $(me).jqxGrid('getselectedrowindexes');
                    if (selectedRowIndexes.length > 0)
                    {
                        // returns the selected row's data.
                        me.selectedRowData = $(me).jqxGrid('getrowdata', selectedRowIndexes[0]);
                    }

                    return false;
                });

                //Define row grid cell click event to open the menu
                me.grid.on('cellclick', function (event) {
                    if (event.args.rightclick) {
                        me.selectedColumnField = event.args.datafield;
                        var scrollTop = $(window).scrollTop();
                        var scrollLeft = $(window).scrollLeft();
                        contextMenu.jqxMenu('open', parseInt(event.args.originalEvent.clientX) + 5 + scrollLeft, parseInt(event.args.originalEvent.clientY) + 5 + scrollTop);

                        //Enable grouping menu
                        $('.datarid-row-menu-add-group-button').removeClass('disabled');
                        for (var i = 0; i < me.groups.length; i++) {
                            //if column is grouped already disable the functionality
                            if (me.selectedColumnField === me.groups[i]) {
                                $('.datarid-row-menu-add-group-button').addClass('disabled');
                                break;
                            }
                        }
                    }
                });

                //Define group change event
                me.grid.on('groupschanged', function (event) {
                    // event arguments.
                    var args = event.args;

                    // set me.groups to groups array.
                    me.groups = args.groups;
                });

                // disable the default browser's context menu.
                $(me).on('contextmenu', '.jqx-grid-content', function (e) {
                    return false;
                });

                //Define each menu buttons' task
                contextMenu.on('itemclick', function (event)
                {
                    // get the clicked LI element.
                    var element = event.args;

                    if ($(element.firstChild).hasClass('datarid-row-menu-delete-button')) {
                        deleteRow(me.selectedRowData);
                    } else if ($(element.firstChild).hasClass('datarid-row-menu-add-group-button')) {
                        addToGroups(me.selectedColumnField);
                    } else if ($(element.firstChild).hasClass('datarid-row-menu-sort-desc-button')) {
                        sortByDesc(me.selectedColumnField);
                    } else if ($(element.firstChild).hasClass('datarid-row-menu-sort-asc-button')) {
                        sortByAsc(me.selectedColumnField);
                    } else if ($(element.firstChild).hasClass('datarid-row-menu-sort-clear-button')) {
                        sortByClear(me.selectedColumnField);
                    } else if ($(element.firstChild).hasClass('datarid-row-menu-edit-button')) {
                        editRow(me.selectedRowData);
                    }
                });
            }
        };

        var addToGroups = function (field) {
            $(me).jqxGrid('addgroup', field);
        };

        var sortByDesc = function (field) {
            $(me).jqxGrid('sortby', field, 'desc');
        };

        var sortByAsc = function (field) {
            $(me).jqxGrid('sortby', field, 'asc');
        };

        var sortByClear = function (field) {
            $(me).jqxGrid('sortby', field, null);
        };

        var editRow = function (row) {
            if (me.canEdit) {
                if (me.redirectEdit) {
                    window.open(me.editUrl.replace('__' + me.settings.source._source.id + '__', me.selectedRowData.uid, '_blank'));
                } else {
                    $.ajax({
                        url: me.editUrl.replace('__' + me.settings.source._source.id + '__', me.selectedRowData.uid),
                        success: function (data) {
                            loadModalWindow(data, function(){$(me).jqxGrid("updatebounddata")});
                        }
                    });
                }
            }
        };

        //Custom events
        var deleteRow = function (row)
        {
            if (me.canDelete)
            {
                //Create a yes/no dialog
                var dialog = new $.gglib.YesNoDialog({
                    title: me.deleteRowTitle,
                    content: me.deleteRowMessage,
                });

                //Register on accept event
                dialog.window.on('dialog-yes-click', function () {
                    dialog.closeWindow();
                    $.ajax({
                        url: me.deleteUrl.replace('__' + me.settings.source._source.id + '__', me.selectedRowData.uid),
                        complete: function (jqXHR, textStatus) {
                            try {
                                var data = jqXHR.responseText;
                                switch (textStatus) {
                                    case 'success':
                                        $(me).jqxGrid("updatebounddata");
                                        break;
                                    case 'error':
                                        $.gglib.Dialog({title: 'Warning!', content: data});
                                        break;
                                    default:
                                        $(me).jqxGrid("updatebounddata");
                                        break;
                                }
                            } catch (err) {

                            }
                        }
                    });
                });
            }
        };

        me.handlers = function () {
            me.grid.on('rowselect', function (event) {
                // event arguments.
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // row's data. The row's data object or null(when all rows are being selected or unselected with a single action). If you have a datafield called "firstName", to access the row's firstName, use var firstName = rowData.firstName;
                var rowData = args.row;

                me.selectedRowData = rowData;

                return false;
            });
        };

        //Starts the grid
        me.build = function () {
            me.grid = $(me).jqxGrid(me.settings);
            me.handlers();
        };

        return me;
    }
}(jQuery));