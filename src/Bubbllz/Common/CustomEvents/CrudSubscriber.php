<?php

namespace Bubbllz\Common\CustomEvents;

use Bubbllz\CategoryBundle\Events\CustomEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Description of CategoryListener
 *
 * 
 */
class CrudSubscriber implements EventSubscriberInterface
{

    /**
     *
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function BeforeSave(ProductEvent $event)
    {

    }

    public static function getSubscribedEvents()
    {
        return array(
            CustomEvents::BEFORE_CREATE => array('BeforeSave', 10),
            PartnershipEvents::AFTER_CREATE => array('AfterSave', 10)
        );
    }

    

}
