<?php

namespace Bubbllz\Common\CustomEvents;

use Bubbllz\Common\CustomEvents\AuditEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Description of CategoryListener
 *
 * 
 */
class AuditSubscriber implements EventSubscriberInterface
{

    /**
     *
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function BeforeSave(AuditEvent $event)
    {
         //$this->funcname($event->getAudit());
//        $this->funcname2($this->container->get('request'), $event->getAudit());
    }

    public static function getSubscribedEvents()
    {
        return array(
            AuditCustomEvents::BEFORE_CREATE => array('BeforeSave', 10),
            AuditCustomEvents:: AFTER_CREATE => array('AfterSave', 10),
            AuditCustomEvents:: BEFORE_UPDATE => array('BeforeUpdate', 10),
            AuditCustomEvents:: AFTER_UPDATE => array('AfterUpdate', 10)
        );
    }

}
