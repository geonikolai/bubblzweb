<?php

namespace Bubbllz\Common\Services;

use Bubbllz\Common\Services\IService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

abstract class Service implements IService
{

    /**
     *
     * @var EntityManager
     */
    protected $em;

    /**
     *
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     *
     * @var ContainerInterface
     */
    protected $container;
    protected $class;

    function __construct(ContainerInterface $container, $class)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->class = $class;
        $this->authorizationChecker = $container->get('security.authorization_checker');
    }

    /**
     * Returns an AccessDeniedException.
     *
     * This will result in a 403 response code. Usage example:
     *
     *     throw $this->createAccessDeniedException('Unable to access this page!');
     *
     * @param string          $message  A message
     * @param \Exception|null $previous The previous exception
     *
     * @return AccessDeniedException
     */
    public function enforceUserSecurity($message, \Exception $previous = null, $userId, $roles)
    {
        foreach ($roles as $role)
        {
            $securityContext = $this->container->get('security.context');
            if ($securityContext->isGranted($role . strtoupper($this->class)))
            {
                throw new AccessDeniedException($message, $previous);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function Create($entity)
    {
        //$this->enforceUserSecurity('ROLES_PERMISSION_CREATE' . $this->class);
        $this->em->persist($entity);
        $this->em->flush($entity);
    }

    /**
     * {@inheritdoc}
     * @return mixed
     */
    public function GetAllNoDefault()
    {
        if ($this->class == "" || $this->class == null)
        {
            return;
        }

        return $this->em->createQueryBuilder()
                        ->select(array('c'))
                        ->from($this->class, 'c')
                        ->where('c.isDefault = :default')
                        ->setParameter('default', false)
                        ->getQuery()
                        ->getResult();
    }

    /**
     * {@inheritdoc}
     * @return mixed
     */
    public function GetAll()
    {
        if ($this->class == "" || $this->class == null)
        {
            return;
        }

        return $this->em->createQueryBuilder()
                        ->select(array('c'))
                        ->from($this->class, 'c')
                        ->getQuery()
                        ->getResult();
    }

    /*     * '
     * {@inheritdoc}
     * @return mixed
     */

    public function GetById($id)
    {
        if ($id == null || $id == 0 || $this->class == "" || $this->class == null)
        {
            return;
        }

//        $this->em->getFilters()->disable('oneLocale');
        return $this->em->createQueryBuilder()
                        ->select('c')
                        ->from($this->class, 'c')
                        ->where('c.id = ?1')
                        ->setParameter(1, $id)
                        ->getQuery()
                        ->getSingleResult();
    }

    /**
     * {@inheritdoc}
     * @return mixed
     */
    public function GetFirst()
    {
        return $this->em->createQueryBuilder()
                        ->select(array('c'))
                        ->from($this->class, 'c')
                        ->getQuery()
                        ->setMaxResults(1)
                        ->getSingleResult();
    }

    /**
     * {@inheritdoc}
     */
    public function Remove($entity)
    {
        $this->em->remove($entity);
        $this->em->flush($entity);
    }

    /**
     * {@inheritdoc}
     * 
     */
    public function Update($entity = null)
    {
        $this->em->flush($entity);
    }

//    protected function BaseJson(JqxGridQueryModel $model, QueryBuilder $result)
//    {
//        $selectStatementParts = $this->GetSelectDQLParts($result, ',');
//
//        //Add groupings. Stin ousia gia kathe group kanw order by opote na exw ta swsta apotelesmata apo to query
//        foreach ($model->groups as $group)
//        {
//            $result->addOrderBy($this->GetAliasOfField($group, $selectStatementParts) . '.' . $group, 'asc');
//        }
//
//        /* @var $filter Filter */
//        foreach ($model->filters as $filter)
//        {
//            switch ($filter->condition)
//            {
//                case "CONTAINS":
//                    $result->andWhere($this->GetAliasOfField($filter->field, $selectStatementParts) . '.' . $filter->field . " like :" . $filter->field);
//                    $result->setParameter($filter->field, "%" . $filter->value . "%");
//                    break;
//            }
//        }
//
//        $withoutLimitsResult = clone($result);
//
//        //Add sortings
//        if (isset($model->sortdatafield))
//        {
//            $result->addOrderBy($this->GetAliasOfField($model->sortdatafield, $selectStatementParts) . '.' . $model->sortdatafield, $model->sortorder);
//        }
//
//        //Add limits
//        if (isset($model->recordstartindex))
//        {
//            $result->setFirstResult($model->recordstartindex);
//            $result->setMaxResults($model->pagesize);
//        }
//
//        $rows = $result->getQuery()->getArrayResult();
//        $numRowsWithoutLimit = $withoutLimitsResult->add('select', 'count(' . $result->getRootAliases()[0] . ')')->getQuery()->getSingleScalarResult();
//
//        return ['TotalRows' => $total = $numRowsWithoutLimit, 'Rows' => $rows];
//    }
//
//    private function GetSelectDQLParts($result, $separator)
//    {
//        $parts = $result->getDQLParts()['select'][0]->getParts()[0];
//        $partsArray = explode($separator, $parts);
//        $final = array();
//        foreach ($partsArray as $p)
//        {
//            //Get the field declaration without the (as) statement if there is any 
//            //p.x. for the statement => select a.id as id -- we need only the first part which means -- a.id -- and remove the -- as id --
//            $tmp = explode(" ", $p)[0];
//            if ($tmp == "")
//            {
//                //if $p is empty string means that we found a space in the begining of the string so we take the second part
//                $tmp = explode(" ", $p)[1];
//            }
//            $aliasFieldArray = explode('.', $tmp);
//            $final[] = ['alias' => $aliasFieldArray[0], 'field' => $aliasFieldArray[1]];
//        }
//        return $final;
//    }
//
//    private function GetAliasOfField($field, $array = [])
//    {
//        foreach ($array as $value)
//        {
//            if ($value['field'] == $field)
//            {
//                return $value['alias'];
//            }
//        }
//        throw new Exception(sprintf("Field %f does not exist on any of the aliases on class GGLib\DomainBundle\Services\Common\Service", $field));
//    }

    public function GetByParameters($params = array())
    {
        $results = $this->GetAll();
        $index = 1;
        foreach ($params as $key => $value)
        {
            $results->andWhere('x.' . $key . ' = ?' . $index);
            $results->setParameter($key, $value);
            $index++;
        }
        return $results->orderBy('x.id', 'desc')->getQuery()->getArrayResult();
    }

    public function SetMessage($type, $message = null)
    {
        if (strpos($message, 'cannot be null') !== false)
            $this->container->get('service_session_manager')->GetSession()->getFlashBag()->add('error', 'Η καταχώρηση απέτυχε, ελλιπή στοιχεία');
        if (strpos($message, 'Duplicate entry') !== false)
            $this->container->get('service_session_manager')->GetSession()->getFlashBag()->add('error', 'Η καταχώρηση απέτυχε, βρέθηκε διπλή εγγραφή');
        if (strpos($message, 'success') !== false)
            $this->container->get('service_session_manager')->GetSession()->getFlashBag()->add($type, 'Αποθηκεύτηκε');
        if (strpos($message, 'general_error') !== false)
            $this->container->get('service_session_manager')->GetSession()->getFlashBag()->add('error', 'Σφάλμα, δεν αποθηκεύτηκε');
        if (strpos($message, 'No result was found') !== false)
            $this->container->get('service_session_manager')->GetSession()->getFlashBag()->add('error', 'Σφάλμα, δεν βρέθηκε εγγραφή');
        if (strpos($message, 'password') !== false)
            $this->container->get('service_session_manager')->GetSession()->getFlashBag()->add('error', 'Απέτυχε η επαλήθευση κωδικού');
        if (strpos($message, 'already_exist') !== false)
            $this->container->get('service_session_manager')->GetSession()->getFlashBag()->add('error', 'To username ή to email υπάρχει ήδη');
    }

    public function IsEmptyOrNull($params)
    {
        foreach ($params as $param)
        {
            if ($param == null || $param == "")
            {
                return false;
            }
        }
        return true;
    }
}
