<?php

namespace Bubbllz\Common\Services;

use Bubbllz\EntitiesBundle\Interfaces\IEntity;

interface IService
{
    /**
     * Pesrists an entity to the database
     * @param IEntity $entity
     */
    public function Create($entity);
    
    /**
     * Returns the first element found in the table. Throws exception \Doctrine\ORM\NoResultException if non found
     */
    public function GetFirst();
    
    /**
     * Updates an entity to the database
     * @param IEntity $entity
     */
    public function Update($entity = null);
    
    /**
     * Removes the entity from the database
     * @param IEntity $entity
     */
    public function Remove($entity);
    
    /**
     * Retrieves all the enities from a table
     */
    public function GetAll();
    
    /**
     * Returns an entity by its id
     * @param mixed $id
     */
    public function GetById($id);
}
