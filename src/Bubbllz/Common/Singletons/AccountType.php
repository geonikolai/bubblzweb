<?php

namespace Bubbllz\Common\Singletons;

class AccountType
{
    
     private function __construct(){}
    
    /**
     * @var instance
     */
    private static $instance;
    
        public static function getInstance()
    {
        if (!self::$instance)
        {
            self::$instance = new AccountType();
        }
        return self::$instance;
    }

  
    const ACCOUNT_TYPE_MULTI_STORE_COMPANY = 'account.type.multi.store.company';
    const ACCOUNT_TYPE_SINGLE_STORE_COMPANY = 'account.type.single.store.company';
    const ACCOUNT_TYPE_BUBBLZ_COMPANY = 'account.type.bubbllz.company';
    const ACCOUNT_TYPE_BRAND_COMPANY = 'account.type.brand.type.company';
    
       // RETURN ACCOUNT TYPES
       public function getAccountTypes()
    {
        return
                [
                    AccountType::ACCOUNT_TYPE_MULTI_STORE_COMPANY => "ACCOUNT_TYPE_MULTI_STORE_COMPANY",
                    AccountType::ACCOUNT_TYPE_SINGLE_STORE_COMPANY => "ACCOUNT_TYPE_SINGLE_STORE_COMPANY",
                    AccountType::ACCOUNT_TYPE_BUBBLZ_COMPANY => "ACCOUNT_TYPE_BUBBLZ_COMPANY",
                    AccountType::ACCOUNT_TYPE_BRAND_COMPANY => "ACCOUNT_TYPE_BRAND_COMPANY",
                ];
    }
}
