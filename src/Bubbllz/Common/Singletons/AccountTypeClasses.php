<?php

namespace Bubbllz\Common\Singletons;

use Bubbllz\Common\Singletons\AccountType;

class AccountTypeClasses
{

    /**
     * @var accountType
     */
    public $accountType;

    /**
     * @var accountTypeClasses
     */
    private $accountTypeClasses;
    
    

    const CLASS_COMPANY = 'class.company';
    const CLASS_ACCOUNT = 'class.account';

    private function __construct()
    {
        
    }

    public static function getInstance()
    {
        if (!self::$accountTypeClasses)
        {
            if ($this->accountType == AccountType::ACCOUNT_TYPE_BUBBLZ_COMPANY)
            {
                $this->$accountTypeClasses = [
                    self::CLASS_ACCOUNT,
                    self::CLASS_COMPANY,
                ];
            }

            if ($this->accountType == AccountType::ACCOUNT_TYPE_BRAND_COMPANY)
            {
                $this->$accountTypeClasses = 
                [
                            self::CLASS_ACCOUNT,
                            self::CLASS_COMPANY,
                ];
            }
            self::$accountTypeClasses = new AccountTypeClasses();
        }
        return self::$accountTypeClasses;
    }

}
