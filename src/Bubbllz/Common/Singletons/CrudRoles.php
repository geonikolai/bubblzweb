<?php

namespace Bubbllz\Common\Singletons;

/**
 * Description of CrudRoles
 *
 * @author George
 */
class CrudRoles
{

    private function __construct()
    {
        
    }

    /**
     * @var CrudRoles
     */
    private static $instance;

    // PERMISSIONS ON CLASS
    const ROLE_CREATE = 'role.view';
    const ROLE_EDIT = 'role.create';
    const ROLE_DELETE = 'role.edit.';

    public static function getInstance()
    {
        if (!self::$instance)
        {
            self::$instance = new CrudRoles();
        }
        return self::$instance;
    }

    public function getCrudRoles()
    {
        return
                [
                    CrudRoles::ROLE_CREATE => "ROLE_CREATE",
                    CrudRoles::ROLE_EDIT => "ROLE_EDIT",
                    CrudRoles::ROLE_DELETE => "ROLE_DELETE",
        ];
    }

}
