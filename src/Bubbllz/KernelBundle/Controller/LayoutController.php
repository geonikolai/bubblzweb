<?php

namespace Bubbllz\KernelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LayoutController extends Controller
{
    
    public function AsideAction()
    {
        return $this->render('BubbllzKernelBundle:Shared:Aside.html.twig');
    }
}
