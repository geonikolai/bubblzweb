<?php

namespace Bubbllz\KernelBundle\Controller;

use Bubbllz\UserBundle\BubbllzUserBundle;
use Bubbllz\UserBundle\Entity\User;
use Bubbllz\UserBundle\Form\UserType;
use Exception;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/profile")
 */
class UserController extends BaseController
{

    protected $indexView = "BubbllzUserBundle:User:Index.html.twig";
    protected $createView = "BubbllzUserBundle:User:Create.html.twig";

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->service = $this->container->get(BubbllzUserBundle::PROFILE_SERVICE);
    }

    /**
     * @Route("/", name="backend_users_index")
     */
    public function IndexAction()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->createQueryBuilder('u')
                ->select("partial u.{id, username, email}, partial g.{id, name}")
                ->leftJoin('u.groups', 'g')
                ->getQuery()
                ->getResult();

        return $this->render('BubbllzUserBundle:User:Index.html.twig', array(
                    'users' => $users
        ));
    }

    /**
     * @Route("/create", name="backend_users_create")
     */
    public function CreateAction(Request $request)
    {
        $this->service = $this->get(BubbllzUserBundle::PROFILE_SERVICE);

        /* @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        /* @var $entity User */
        $entity = $userManager->createUser();


        $form = $this->createForm(UserType::class, $entity);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            try
            {
                $userManager->updateUser($entity);
                return $this->redirectToRoute('backend_users_create');
            } catch (Exception $ex)
            {
                return $this->redirectToRoute('backend_users_create');
            }
        }

        return $this->render('BubbllzUserBundle:User:Create.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/edit/{id}", name="backend_users_edit")
     */
    public function EditAction(Request $request, $id = 0)
    {
        $this->service = $this->get(BubbllzUserBundle::PROFILE_SERVICE);

        /* @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        /* @var $entity User */
        $entity = $userManager->findUserBy(['id' => $request->get('id')]);



        $form = $this->createForm(UserType::class, $entity);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            try
            {
                $userManager->updateUser($entity);
                return $this->redirectToRoute('backend_users_create');
            } catch (Exception $ex)
            {
             return $this->redirectToRoute('backend_users_create');
            }
        }

        return $this->render('BubbllzUserBundle:User:Create.html.twig', array(
                    'form' => $form->createView()
        ));
    }

}
