<?php

namespace Bubbllz\KernelBundle\Controller;

use Bubbllz\EntitiesBundle\Entity\Account;
use Bubbllz\EntitiesBundle\Form\AccountType;
use Bubbllz\KernelBundle\BubbllzKernelBundle;
use Bubbllz\UserBundle\BubbllzUserBundle;
use Bubbllz\UserBundle\Entity\User;
use Bubbllz\UserBundle\Form\UserType;
use Exception;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/account")
 */
class AccountController extends BaseController
{

    protected $indexView = "BubbllzUserBundle:User:Index.html.twig";
    protected $createView = "BubbllzUserBundle:User:Create.html.twig";

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->service = $this->container->get(BubbllzKernelBundle::ACCOUNT_SERVICE);
    }

    /**
     * @Route("/", name="kernel_users_index")
     */
    public function IndexAction()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->createQueryBuilder('u')
                ->select("partial u.{id, username, email}, partial g.{id, name}")
                ->leftJoin('u.groups', 'g')
                ->getQuery()
                ->getResult();

        return $this->render('BubbllzUserBundle:User:Index.html.twig', array(
                    'users' => $users
        ));
    }

    /**
     * @Route("/create", name="kernel_users_create")
     */
    public function CreateAction(Request $request)
    {
        $this->service = $this->get(BubbllzKernelBundle::ACCOUNT_SERVICE);
        $entity = new Account();

        $form = $this->createForm(new AccountType(), $entity);
        if ($request->getMethod() == 'POST')
        {
            $form->handleRequest($request);


//            if ($this->CheckUserNameExists($entity->getUser()) == true || $this->CheckEmailExists($entity->getUser()))
//            {
//                $this->service->SetMessage('error', 'already_exist');
//                return $this->redirectToRoute('kernel_users_create', array());
//            }

            if ($request->get('bubbllz_entitiesbundle_account')['user']['plainPassword']['first'] != $request->get('bubbllz_entitiesbundle_account')['user']['plainPassword']['second'])
            {
                $this->service->SetMessage('error', 'password mismatch');
                return $this->redirectToRoute('kernel_users_create', array());
            }
            if ($form->isValid())
            {

                if ($this->service->IsEmptyOrNull(array(
                            $entity->getUser()->getEmail(),
                            $entity->getUser()->getUsername(),
                            $entity->getUser()->getPlainPassword(),
                            $entity->getUser()->getGroups()
                        )))
                {

                    $allias = $entity->getFirstName() . ' ' . $entity->getLastName();
                    $entity->setFullName($allias);



                    /* @var $userManager UserManagerInterface */
                    $userManager = $this->get('fos_user.user_manager');
                    $entity->getUser()->setEnabled(true);
                     $userManager->updateUser($entity->getUser());
                   
                    $this->service->Create($entity);

                    try
                    {

                        $this->service->Create($entity);
                        $this->service->SetMessage('success', 'success');
                        return $this->redirect($this->generateUrl('kernel_users_create', array()));
                    } catch (Exception $e)
                    {
                        $this->service->SetMessage('error', $e->getMessage());
                        return $this->redirectToRoute('kernel_users_create', array());
                    }
                }
            }
        }


        return $this->render('BubbllzUserBundle:User:Create.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/edit/{id}",  name="kernel_users_edit")
     */
    public function EditAction(Request $request, $id = 0)
    {

        $entity = new Account();


        $this->service = $this->get(BubbllzKernelBundle::ACCOUNT_SERVICE);

        /* @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        /* @var $entity User */
        //$entity = $userManager->findUserBy(['id' => $request->get('id')]);



        $form = $this->createForm(AccountType::class, $entity);


        $form->handleRequest($request);
        var_dump($entity);
        die;
        if ($form->isSubmitted() && $form->isValid())
        {
            try
            {
                $userManager->updateUser($entity);
                return $this->redirectToRoute('kernel_users_edit');
            } catch (Exception $ex)
            {
                return $this->redirectToRoute('kernel_users_edit');
            }
        }

        return $this->render('BubbllzUserBundle:User:Create.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    public function CheckUserNameExists($user)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        if ($userManager->findUserBy(array('usernameCanonical' => $this->canonicalizeUsername($user->getUsername()))) != null)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public function CheckUserOnUpdate($newuser, $olduser)
    {
        $check1;
        $check2;
        $userManager = $this->container->get('fos_user.user_manager');
        if ($userManager->canonicalizeUsername($newuser->getUsername()) == $userManager->canonicalizeUsername($olduser->getUsername()))
        {
            $check1 = false;
        } else
        {
            if ($userManager->CheckUserNameExists($newuser) == true)
            {
                $check1 = true;
            } else
            {
                $check1 = false;
            }
        }

        if ($userManager->canonicalizeEmail($newuser->getEmail()) == $userManager->canonicalizeEmail($olduser->getEmail()))
        {
            $check2 = false;
        } else
        {
            if ($userManager->CheckEmailExists($newuser) == true)
            {
                $check2 = true;
            } else
            {
                $check2 = false;
            }
        }

        if ($check1 == false && $check2 == false)
        {
            return false;
        } else
        {
            return true;
        }
    }

}
