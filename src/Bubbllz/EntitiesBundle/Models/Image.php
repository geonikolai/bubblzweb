<?php

namespace Bubbllz\EntitiesBundle\Entity;

use Bubbllz\Common\Configuration\FileNameSanitizer;
use Bubbllz\EntitiesBundle\Interfaces\IFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="bubbllz_images")
 */
class Image implements IFile, IAuditEntity
{
    use \Bubbllz\EntitiesBundle\Traits\EntityTrait;
    use \Bubbllz\EntitiesBundle\Traits\AuditTrait;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    
    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * Set path
     *
     * @param string $path
     * @return Image
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads';
    }
    
    /**
     * Saves a file to the server and sets audit properties
     * 
     * @param string $directory (optional) The directory to upload the file
     * @return mixed
     */
    public function upload($directory = '')
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile())
        {
            return;
        }
        
        //Sanitize the file name
        $filename = FileNameSanitizer::sanitaze($this->getFile()->getClientOriginalName());
        
        //Set upload directory merged with filename
        $fileUrl = '/' . $this->getUploadDir () . $directory . '/' . $filename;
        
        //Upload the file
        $this->getFile()->move($this->getUploadRootDir () . $directory, $filename);

        // set the path property to the filename where you've saved the file
        $this->path = $fileUrl;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }
}
