<?php

namespace Bubbllz\EntitiesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('firstName', null, array(
                    'attr' => array(
                        'class' => 'input full-width',
                    ),
                    'required' => false,
                        )
                )
                ->add('lastName', null, array(
                    'attr' => array(
                        'class' => 'input  full-width',
                    ),
                    'required' => false,
                        )
                )
                ->add('phone', null, array(
                    'attr' => array(
                        'class' => 'input  full-width',
                    ),
                    'required' => false,
                        )
                )
                ->add('phone2', null, array(
                    'attr' => array(
                        'class' => 'input  full-width',
                    ),
                    'required' => false,
                        )
                )
                ->add('address', null, array(
                    'attr' => array(
                        'class' => 'input  full-width',
                    ),
                    'required' => false,
                        )
                )
                ->add('isActive')
                ->add('deleted')
                ->add('user', new \Bubbllz\UserBundle\Form\UserType('Bubbllz\UserBundle\Entity\User'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bubbllz_entitiesbundle_account';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bubbllz\EntitiesBundle\Entity\Account'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bubbllz_entitiesbundle_account';
    }

}
