<?php

namespace Bubbllz\EntitiesBundle\Interfaces;

use DateTime;

/**
 * Exposes auditory functions
 */
interface IAuditEntity 
{

    public function GetCreatedDate();

    public function GetCreatedBy();

    public function GetModifiedDate();

    public function GetModifiedBy();

    public function SetCreatedDate(DateTime $createdDate);

    public function SetCreatedBy($createdBy);

    public function SetModifiedDate(DateTime $modifiedDate);

    public function SetModifiedBy($modifiedBy);
}
