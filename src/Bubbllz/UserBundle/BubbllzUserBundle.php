<?php

namespace Bubbllz\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BubbllzUserBundle extends Bundle
{
      const PROFILE_SERVICE = "bubbllz_user.profile_service";
      const GROUP_SERVICE =  "userbundle.group_service";
    
    public function __construct()
    { 
        
        
       
    }
    
    public function getParent()
    {
        return 'FOSUserBundle';
    }
    
    
}
