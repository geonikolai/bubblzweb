<?php

namespace Bubbllz\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LayoutController extends Controller
{
    
    public function AsideAction()
    {
        return $this->render('BubbllzUserBundle:Shared:Aside.html.twig');
    }
}
