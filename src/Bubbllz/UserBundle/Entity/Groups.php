<?php

namespace Bubbllz\UserBundle\Entity;

use Bubbllz\EntitiesBundle\Traits\AuditTrait;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\ORM\Mapping as ORM;
use Common\Singletons\AccountType;

/**
 * Groups
 *
 * @ORM\Entity
 * @ORM\Table(name="bubbllz_groups")
 */
class Groups extends BaseGroup
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    use \Bubbllz\EntitiesBundle\Traits\AuditTrait;

    
      /**
     * @var string
     *
     * @ORM\Column(name="account_type", type="string", length=255, nullable=false)
     */
    protected $accountType;

    /**
     * @var integer
     *
     * 
     * @param string $name
     * @param Bubblz/Common/Singletons/CrudRoles $roles
     */
    public function __construct($name, $roles = array())
    {
        parent::__construct($name, $roles);
    }

    /**
     * Set $accountType
     *
     * @param string $accountType
     *
     * @return string
     */
    public function setAccountType($accountType)
    {
        $this->accountType = $accountType;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    public function __toString()
    {
        return $this->name;
    }

}
