<?php

namespace Bubbllz\UserBundle\Events;

use Bubbllz\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Description of ProfileEvent
 *
 * 
 */
class ProfileEvent extends Event
{
    /**
     *
     * @var User
     */
    protected $profile;
    
    public function __construct(User &$profile)
    {
        $this->profile = $profile;
    }
    
    /**
     * 
     * @return User
     */
    public function &getProfile()
    {
        return $this->profile;
    }
}
