<?php

namespace Bubbllz\CategoryBundle\Events;

use Bubbllz\UserBundle\Events\ProfileEvent;
use Bubbllz\UserBundle\Events\ProfileCustomEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Description of CategoryListener
 *
 * 
 */
class ProfileSubscriber implements EventSubscriberInterface
{

    /**
     *
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function BeforeSave(ProfileEvent $event)
    {

    }

    public static function getSubscribedEvents()
    {
        return array(
            PartnershipEvents::PROFILE_REQUEST_BEFORE_CREATE => array('BeforeSave', 10),
            PartnershipEvents::PROFILE_REQUEST_AFTER_CREATE => array('AfterSave', 10)
        );
    }

    

}
