<?php

/**
 * Description of SecurityListener
 *
 * @author GeorgeG
 */

namespace Bubbllz\UserBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class SecurityListener implements AuthenticationSuccessHandlerInterface
{
    /**
     *
     * @var Router
     */
    private $router;
    
    public function __construct(Router $router)
   {
      $this->router = $router;
   }

   public function onAuthenticationSuccess(Request $request, TokenInterface $token)
   {
       /* @var $user User */
       $user = $token->getUser();
       if($user->hasRole("ROLE_SUPER_ADMIN"))
       { 
           $response = new RedirectResponse($this->router->generate("kernel_dashboard_index"));
       }
       else if($user->hasRole("ROLE_BRAND"))
       {
           $response = new RedirectResponse($this->router->generate("kernel_dashboard_index"));
       }
       else
       {
           $response = new RedirectResponse($this->router->generate("kernel_dashboard_index"));
       }
       
       return $response;
   }
}
