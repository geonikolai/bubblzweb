<?php

namespace Bubbllz\HomeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name', null, array(
                    'attr' => array(
                        'label' => false ,
                        'placeholder' => 'Ονομα',
                    ),
                    'label' => false
                ))
                 ->add('email', null, array(
                    'attr' => array(
                        'label' => false ,
                        'placeholder' => 'Email',
                    ),
                     'label' => false
                ))
                ->add('message', TextareaType::class, array(
                    'attr' => array(
                        'rows' => 4,
                        'class' => 'form-control',
                        'placeholder' => 'Μήνυμα',
                    ),
                    'label' => false
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bubbllz\HomeBundle\Models\ContactForm'
        ));
    }

}
