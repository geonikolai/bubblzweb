<?php

namespace Bubbllz\HomeBundle\Controller;

use Bubbllz\Common\Helpers\Validator;
use Bubbllz\EntitiesBundle\Entity\TalkClient;
use Bubbllz\EntitiesBundle\Form\TalkClientType;
use Bubbllz\HomeBundle\BubbllzHomeBundle;
use Bubbllz\HomeBundle\Form\ContactFormType;
use Bubbllz\HomeBundle\Models\ContactForm;
use Bubbllz\HomeBundle\Services\HomeService;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swift_Message;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/")
 */
class HomeController extends BaseController
{

    protected $indexView = "BubbllzHomeBundle:Home:Home.html.twig";

    /**
     * @var HomeService
     */
    protected $service;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->service = $this->container->get(BubbllzHomeBundle::HOME_SERVICE);
    }

    /**
     * @Route("/", name="home")
     */
    public function HomeAction()
    {
        $entity = new ContactForm();
        $form = $this->createForm(new ContactFormType(), $entity, array());

        return $this->render($this->indexView, array(
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/business", name="home_business")
     */
    public function BusinessAction(Request $request)
    {

        $entity = new TalkClient();
        $form = $this->createForm(TalkClientType::class, $entity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            if (Validator::IsNullOrEmptyString($entity->getFullName()))
            {
                $message = ['error', 'Παρακαλώ συμπληρώστε το όνομά σας'];
                return $this->render('BubbllzHomeBundle:Home:ContactPopup.html.twig', array(
                            'message' => $message
                ));
            }

            if (Validator::IsNullOrEmptyString($entity->getContactPhone()))
            {
                $message = ['error', 'Παρακαλώ συμπληρώστε το Τηλέφωνο σας'];
                return $this->render('BubbllzHomeBundle:Home:ContactPopup.html.twig', array(
                            'message' => $message
                ));
            }

            try
            {
                $swift_message = Swift_Message::newInstance();
                $swift_message->setSubject('Message from Name of the sender:' . $entity->getFullName());
                $swift_message->setFrom($entity->getEmail());
                $swift_message->setTo('support@bubblz.com');
                $swift_message->setBody('Message from Name of the sender:' . $entity->getFullName());
                $this->get('mailer')->send($swift_message);

                $this->service->Create($entity);
                $message = ['success', 'Ευχαριστουμε για την Εκδήλωση Ενδιαφέροντος'];
                
                $this->get('session')->getFlashBag()->set('submitted', true);
                return $this->redirectToRoute("home_business_success");
            } catch (Exception $ex)
            {
                $message = ['error', 'Μη Έγκυρη Διεύθυνση Email '];
                return $this->redirectToRoute("home_business");
            }
        };
        
        return $this->render('BubbllzHomeBundle:Home:Business.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    /**
     * @Route("/business/success", name="home_business_success")
     */
    public function BusinessSuccessAction()
    {
        return $this->render("BubbllzHomeBundle:Home:BusinessSuccess.html.twig", array(
        ));
    }

    /**
     * @Route("/sent-info-email", name="home_sent_info_mail")
     */
    public function SentInfoEmailAction(Request $request)
    {
        $entity = new ContactForm;
        $form = $this->createForm(new ContactFormType(), $entity, array());
        $message = ['', ''];
        try
        {
            $form->handleRequest($request);
            if (Validator::IsNullOrEmptyString($entity->getName()))
            {
                $message = ['error', 'Παρακαλώ συμπληρώστε το όνομά σας'];
                return $this->render('BubbllzHomeBundle:Home:ContactPopup.html.twig', array(
                            'message' => $message
                ));
            }

            if (Validator::IsNullOrEmptyString($entity->getEmail()))
            {
                $message = ['error', 'Παρακαλώ συμπληρώστε το Email σας'];
                return $this->render('BubbllzHomeBundle:Home:ContactPopup.html.twig', array(
                            'message' => $message
                ));
            }


            if ($form->isSubmitted() && $form->isValid())
            {

                $swift_message = Swift_Message::newInstance();
                $swift_message->setSubject('Message from Name of the sender:' . $entity->getMessage());
                $swift_message->setFrom($entity->getEmail());
                $swift_message->setTo('support@bubblz.com');
                $swift_message->setBody('Message :' . $entity->getMessage());
                $this->get('mailer')->send($swift_message);

                $message = ['success', 'Ευχαριστουμε για την Εκδήλωση Ενδιαφέροντος'];
            }
        } catch (Exception $ex)
        {

            $message = ['error', 'Μη Έγκυρη Διεύθυνση Email '];
        };

        return $this->render('BubbllzHomeBundle:Home:ContactPopup.html.twig', array(
                    'message' => $message
        ));
    }

}
